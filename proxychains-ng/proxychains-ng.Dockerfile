FROM --platform=$TARGETPLATFORM alpine:latest

RUN apk add --no-cache git build-base linux-headers \
      && git clone https://github.com/rofl0r/proxychains-ng.git \
      && cd proxychains-ng \
      && ./configure --prefix=/usr --sysconfdir=/etc \
      && make \
      && make install \
      && cd .. \
      && rm -rf proxychains-ng \
      && apk del git build-base linux-headers

CMD ["proxychains4", "-f", "/etc/proxychains4.conf"]